---
title: 实用软件工程
subtitle: CH04 系统设计
author: 滨州学院
theme: white
---

## 学习目标

- 掌握：概要设计和详细设计的基本任务；数据库设计的方法。
- 熟悉：系统设计的基本概念和工具。

# 4.1 系统设计的基本概念

## 软件设计

软件设计
  ~ 软件设计是一个把软件需求转换为软件表示的过程。
  ~ 系统设计的基本目标是用比较抽象和概括的方式确定目标系统如何完成预定的任务，
  ~ 软件设计是确定系统的物理模型。

## 系统设计的重要性

- 占据软件项目开发成本的绝大部分
- 开发阶段最重要的步骤
- 设计决策影响软件实现的成败
- 是软件工程和软件维护的基础

## 系统设计的内容

- 软件结构设计
  - 定义软件系统各主要部件之间的关系
- 数据设计
  - 将分析时创建的模型转化为数据结构的定义
- 接口设计
  - 描述软件内部、软件和协作系统之间以及软件与人之间如何通信
- 过程设计
  - 将系统结构部件转换成软件的过程性描述

## 系统设计的总体步骤

- 概要设计
  - 将软件需求转化为软件体系结构
  - 确定系统级接口、全局数据结构或数据库模式
- 详细设计
  - 确立每个模块的实现算法和局部数据结构
  - 用适当的方法表示算法和数据结构的细节
- 软件设计的迭代过程
  - 先进行高层次结构设计
  - 再进行低层次的过程设计
  - 最后穿插进行数据设计和接口设计

# 4.2 系统设计的目的和任务

## 概要设计的基本任务

- 1）设计软件系统结构
- 2）进行数据结构及数据库设计
- 3）编写概要设计文档
- 4）评审

## 详细设计的基本任务

- 1）对每个模块进行详细的算法设计
- 2）对模块内的数据结构进行设计
- 3）对数据库进行物理设计，确定数据的物理结构
- 4）其他设计：代码设计；输入、输出格式设计；人机对话设计
- 5）编写详细设计说明书
- 6）评审

# 4.3 概要设计

## 1. 概要设计原理

- 模块和模块化
- 抽象与逐步求精
- 信息隐藏
- 模块独立性

## 模块和模块化

模块
  ~ 是指具有相对独立性的，有数据说明、执行语句等程序对象构成的集合。
  ~ 每个模块单独命名，通名称访问指定模块
  ~ 高级语言中表现为函数、子程序、过程等

- - -

模块的四个特征
  ~ 输入输出：模块需要和产生的信息
  ~ 功能：模块所做的工作
  ~ 内部数据：仅供该模块本身引用的数据
  ~ 程序代码：模块通过程序代码完成其功能


模块的内部特征和外部特征
  ~ 外部特征：输入输出、功能
  ~ 内部特征：内部数据、程序代码

- - -

模块化
  ~ 是指将整个程序划分为若干个模块，每个模块用于实现一个特定的功能。
  ~ 划分模块可以降低解决问题的难度 $C(P_1+P_2) > C(P_1) + C(P_2)$
  ~ 随着模块数目增加，设计模块接口所需工作量也将增加，应适当控制模块数目


## 抽象与逐步求精

抽象
  ~ 就是抽出事物的本质特性而暂时不考虑它们的细节。

逐步求精
  ~ 为了集中精力解决主要问题而尽量推迟对问题细节的考虑。

## 信息隐藏

信息隐藏
  ~ 是指在设计和确定模块时，使得一个模块包含的信息对于不需要这些信息的其他模块来说是不能访问的。

## 模块独立性

模块独立性
  ~ 是指软件系统中的每个模块只涉及软件要求的具体子功能，而和软件系统中其他模块之间的联系最小且接口是简单的。
  ~ 模块独立性强，则信息隐藏性好，模块的可理解性、可维护性、可测试性好，可靠性也好
  ~ 独立性强的模块易于开发，可并行工作，生产率高
  ~ 模块的独立性使用耦合性和内聚性进行度量


## 耦合性

耦合性
  ~ 是模块之间相互连接的紧密程度的度量。从低到高分别是：
  ~ 无直接耦合、数据耦合、标记耦合、控制耦合、公共耦合、内容耦合
  ~ 软件设计中应追求尽可能松散的耦合系统。

## 内聚性

内聚性
  ~ 是度量一个模块内部各个组成元素之间相互结合的紧密程度的指标。由低到高分别是：
  ~ 偶然内聚、逻辑内聚、时间内聚、通信内聚、顺序内聚、功能内聚
  ~ 优秀的软件设计应尽量做到高内聚、低耦合

## 2. 软件结构优化准则

- 模块的分解尽量做到“高内聚，低耦合”。
- 模块结构的深度、宽度、扇出和扇入应适当。
- 模块的作用范围应该在控制范围内。
- 模块接口设计要简单，以便降低复杂程度和冗余度。
- 适当设置模块规模，以保持其独立性。
- 模块功能应该可以预测。


## 3. 软件结构设计的图形工具

- 软件结构图
- 层次图
- HIPO 图

## 软件结构图

- 基本符号：模块、模块的调用关系和辅助符号
- 模块：每个方框代表一个模块
- 模块的调用关系：方框之间的箭头（或直线）表示模块之间的调用关系
    - 调用模块在上，被调用模块在下
    - 可以用箭头表示调用过程中传递的信息
- 辅助符号：弧形箭头表示循环调用，菱形表示选择或者条件调用

## 层次图

- 层次图用来描绘软件的层次结构
- 每个方框表示一个模块
- 矩形框之间的关系表示调用关系
- 适用于自顶向下设计软件结构的过程

## HIPO 图

- 层次图加输入/处理/输出图
- 在 H 图（层次图）中用矩形框表示模块
- 对各子模块编号
- 每个矩形框还要对应一张 IPO 图描绘这个模块的处理过程

## 4. 面向数据流的设计方法

- 典型的数据流类型有两种：
  - 变换型
  - 事务型

- - -

变换型
  ~ 信息沿输入通路进入系统，同时由外部形式变换成内部形式，进入系统的信息通过变换中心，经过加工后再沿输出通路变换成外部形式离开软件系统。

- - -

事务型
  ~ 数据沿输入通路到达一个处理，这个处理根据输入数据的类型在若干个动作序列中选出一个来执行，这种“以事务为中心”的数据流称为“事务流”。


## 5. 软件体系结构设计

软件体系结构
  ~ 软件体系结构为软件系统提供了一个结构、行为和属性的高级抽象，由构成系统的元素的描述、这些元素之间的相互作用、指导元素集成的模式以及这些模式的约束组成。

## 软件体系结构的分类

- 主机-终端体系结构
- 客户-服务器（C/S）体系结构
- 浏览器-服务器（B/S）体系结构
- 三层 C/S 软件体系结构

## B/S 结构相对于 C/S 结构的对比


- 开发和维护成本
- 客户端负载
- 灵活性
- 移植性
- 用户界面风格

## 概要设计说明书

- 概要设计说明书又称为系统设计说明书。包括
  - 软件系统的基本处理流程、软件系统的组织结构、模块划分、功能分配、接口设计、运行设计、数据结构设计、出错处理设计等。

# 4.4 详细设计

## 详细设计

- 概要设计阶段将软件系统分解成多个模块，并确定每个模块的外部特征
  - 功能（做什么）和界面（输入和输出）
- 详细设计阶段要确定每个模块的内部特征
  - 每个模块内部的执行过程（怎样做）

## 详细设计阶段的描述工具
  - 图形：流程图、盒图和问题分析图等
  - 语言：各种程序设计语言
  - 表格：判定表等

## 1. 结构化程序设计方法

- 1969年，Dijkstra 提出了结构化程序设计方法：
  - 以模块化设计为中心
  - 将待开发系统划分为若干相互独立的模块

## 结构化程序设计方法的基本要点

- 1）采用自顶向下、逐步求精的程序设计方法和“单入口、单出口”的控制结构。
- 2）使用三种基本控制结构（顺序、选择和循环）构造程序。
- 3）开发支持库，文档库由专职资料员来维护。
- 4）采用主程序员组的组织形式。


## 2. 详细设计描述工具

- 流程图
- 盒图（N-S 图）
- 问题分析图（PAD 图）
- 过程设计语言（PDL）

## 3. 用户界面设计

- 好的用户界面
- 用户界面设计原则
- 用户界面设计过程
- 人机界面设计指南

## 4. Jackson 方法

Jackson 方法
  ~ 是一种面向数据结构的方法
  ~ 适用于数据处理类问题
  ~ 目标：获得简单清晰的设计方案
  ~ 设计原则：使程序结构同数据结构相对应
  ~ 描述工具：Jackson 图

- - -

- Jackson 图描述就数据结构：
  - 方框表示数据
- Jackson 图描述程序结构：
  - 方框表示模块

## Jackson 图

- 顺序结构
  - 数据由一个或多个数据元素组成，每个元素按确定的次序出现一次
- 选择结构
  - 包含两个或多个数据元素，每次使用这个数据时按一定条件从这些数据元素中选择一个
  - 右上角用圆圈做标记
- 重复结构
  - 数据根据使用时的条件由出现零次或多次的数据元素组成
  - 右上角用星号标记

## Jackson 方法的优缺点

- 优点：简单，易学易用，适用于规模不大的数据处理系统
- 缺点：应用于较大的系统比较困难

## Jackson 方法的设计步骤

- 1）分析并确定输入数据和输出数据的逻辑结构，并用 Jackson 图描述；
- 2）找出输入数据结构和输出数据结构中的对应关系；
- 3）由 Jackson 图导出程序结构；
- 4）列出所有的动作和条件，加入到程序图的适当位置；
- 5）用伪代码（PDL）表示。

## 5. 详细设计说明书

- 对各个具体模块、类等局部元素的设计描述

# 4.5 数据库设计

## 1. 数据库设计的目标

- 功能需求
  - 存储数据及其联系
  - CRUD 等操作
- 性能需求
  - 良好的存储结构、数据共享、数据完整性、数据一致性及安全保密性

## 2. 数据库设计的步骤

- 数据库设计的步骤
- 数据库设计各阶段的主要任务

## 3. 数据库设计的内容

- 数据库概念设计
- 数据库逻辑设计
- 数据库物理设计

## 数据库概念设计

- 分析数据间内在的语义关联
  - ER 图：用属性、实体以及它们之间的联系描述信息

## ER图
  - 实体：客观存在的、相互区别的事物，实体可以是具体的对象
    - 用矩形框表示，框内标注实体名称
  - 属性：实体所具有的性质
    - 用椭圆框表示，框内标注属性名称
  - 联系：实体之间的联系
    - 有一对一(1:1)、一对多(1:n)和多对多(m:n)三种联系
    - 用菱形框表示，框内标注联系名称

## 数据库逻辑设计

- 将概念结构转换成特定 DBMS 所支持的数据模块

## 将 ER 图转换成关系模型

- 主键：为每个关系指定一个或一组属性唯一标识该关系的元组（记录）
- 外键：一个关系的主键在另一个关系中重复出现，标识两个关系的联系

## 转换原则

- 每个实体都对应转换为一个关系，实体中的属性对应设置为关系的字段
- 两个实体存在 1:1 联系：在其中一个关系中设置外键
- 两个实体存在 1:n 联系：把实体联系“1”一方的实体主键纳入“n”一方实体作为外键
- 两个实体存在 m:n 联系：需为“联系”单独建立一个关系，其中必须包含由它联系的两个实体的主键

## 数据库物理设计

- 对于给定的逻辑数据模型，选取一个最适合应用环境的物理结构

